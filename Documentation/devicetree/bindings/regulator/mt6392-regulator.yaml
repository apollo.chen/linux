# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/regulator/mt6392-regulator.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Regulator driver for MT6392 IC from MediaTek Inc.

maintainers:
  - Fabien Parent <fparent@baylibre.com>

description: |
  list of regulators provided by this controller, must be named
  after their hardware counterparts

patternProperties:
  "^buck-v(proc)|(core)|(sys)$":
    $ref: "regulator.yaml#"

  "^ldo-v(xo22)|(aud22)|(cama)|(aud28)|(adc18)|(cn35)|(io28)|(usb)|(mc)|(mch)|(emc3v3)|(gp1)|(gp2)|(cn18)|(camaf)|(m)|(io18)|(camd)|(camio)|(m25)|(efuse)$":
    $ref: "regulator.yaml#"

additionalProperties: false

examples:
  - |
    regulator {
        mt6392_vproc_reg: buck-vproc {
            regulator-name = "buck_vproc";
            regulator-min-microvolt = < 700000>;
            regulator-max-microvolt = <1350000>;
            regulator-ramp-delay = <12500>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vsys_reg: buck-vsys {
            regulator-name = "buck_vsys";
            regulator-min-microvolt = <1400000>;
            regulator-max-microvolt = <2987500>;
            regulator-ramp-delay = <25000>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vcore_reg: buck-vcore {
            regulator-name = "buck_vcore";
            regulator-min-microvolt = < 700000>;
            regulator-max-microvolt = <1350000>;
            regulator-ramp-delay = <12500>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vxo22_reg: ldo-vxo22 {
            regulator-name = "ldo_vxo22";
            regulator-min-microvolt = <2200000>;
            regulator-max-microvolt = <2200000>;
            regulator-enable-ramp-delay = <110>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vaud22_reg: ldo-vaud22 {
            regulator-name = "ldo_vaud22";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <2200000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vcama_reg: ldo-vcama {
            regulator-name = "ldo_vcama";
            regulator-min-microvolt = <2800000>;
            regulator-max-microvolt = <2800000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vaud28_reg: ldo-vaud28 {
            regulator-name = "ldo_vaud28";
            regulator-min-microvolt = <2800000>;
            regulator-max-microvolt = <2800000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vadc18_reg: ldo-vadc18 {
            regulator-name = "ldo_vadc18";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <1800000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vcn35_reg: ldo-vcn35 {
            regulator-name = "ldo_vcn35";
            regulator-min-microvolt = <3300000>;
            regulator-max-microvolt = <3600000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vio28_reg: ldo-vio28 {
            regulator-name = "ldo_vio28";
            regulator-min-microvolt = <2800000>;
            regulator-max-microvolt = <2800000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vusb_reg: ldo-vusb {
            regulator-name = "ldo_vusb";
            regulator-min-microvolt = <3300000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vmc_reg: ldo-vmc {
            regulator-name = "ldo_vmc";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
            regulator-boot-on;
        };

        mt6392_vmch_reg: ldo-vmch {
            regulator-name = "ldo_vmch";
            regulator-min-microvolt = <3000000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
            regulator-boot-on;
        };

        mt6392_vemc3v3_reg: ldo-vemc3v3 {
            regulator-name = "ldo_vemc3v3";
            regulator-min-microvolt = <3000000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
            regulator-boot-on;
        };

        mt6392_vgp1_reg: ldo-vgp1 {
            regulator-name = "ldo_vgp1";
            regulator-min-microvolt = <1200000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vgp2_reg: ldo-vgp2 {
            regulator-name = "ldo_vgp2";
            regulator-min-microvolt = <1200000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vcn18_reg: ldo-vcn18 {
            regulator-name = "ldo_vcn18";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <1800000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vcamaf_reg: ldo-vcamaf {
            regulator-name = "ldo_vcamaf";
            regulator-min-microvolt = <1200000>;
            regulator-max-microvolt = <3300000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vm_reg: ldo-vm {
            regulator-name = "ldo_vm";
            regulator-min-microvolt = <1240000>;
            regulator-max-microvolt = <1390000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vio18_reg: ldo-vio18 {
            regulator-name = "ldo_vio18";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <1800000>;
            regulator-enable-ramp-delay = <264>;
            regulator-always-on;
            regulator-boot-on;
        };

        mt6392_vcamd_reg: ldo-vcamd {
            regulator-name = "ldo_vcamd";
            regulator-min-microvolt = <1200000>;
            regulator-max-microvolt = <1800000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vcamio_reg: ldo-vcamio {
            regulator-name = "ldo_vcamio";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <1800000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vm25_reg: ldo-vm25 {
            regulator-name = "ldo_vm25";
            regulator-min-microvolt = <2500000>;
            regulator-max-microvolt = <2500000>;
            regulator-enable-ramp-delay = <264>;
        };

        mt6392_vefuse_reg: ldo-vefuse {
            regulator-name = "ldo_vefuse";
            regulator-min-microvolt = <1800000>;
            regulator-max-microvolt = <2000000>;
            regulator-enable-ramp-delay = <264>;
        };
...
