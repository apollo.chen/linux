#include <dt-bindings/regulator/mediatek,mt6360-regulator.h>

&mt6360 {
	interrupt-controller;
	interrupt-parent = <&pio>;
	interrupts = <101 IRQ_TYPE_EDGE_FALLING>;
	interrupt-names = "IRQB";

	charger {
		compatible = "mediatek,mt6360-chg";
		richtek,vinovp-microvolt = <14500000>;

		otg_vbus_regulator: usb-otg-vbus-regulator {
			regulator-compatible = "usb-otg-vbus";
			regulator-name = "usb-otg-vbus";
			regulator-min-microvolt = <4425000>;
			regulator-max-microvolt = <5825000>;
		};
	};

	regulator {
		compatible = "mediatek,mt6360-regulator";
		LDO_VIN3-supply = <&mt6360_buck2>;

		mt6360_buck1: buck1 {
			regulator-compatible = "BUCK1";
			regulator-name = "mt6360,buck1";
			regulator-min-microvolt = <300000>;
			regulator-max-microvolt = <1300000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP
						   MT6360_OPMODE_ULP>;
		};

		mt6360_buck2: buck2 {
			regulator-compatible = "BUCK2";
			regulator-name = "mt6360,buck2";
			regulator-min-microvolt = <300000>;
			regulator-max-microvolt = <1300000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP
						   MT6360_OPMODE_ULP>;
		};

		mt6360_ldo1: ldo1 {
			regulator-compatible = "LDO1";
			regulator-name = "mt6360,ldo1";
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <3600000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};

		mt6360_ldo2: ldo2 {
			regulator-compatible = "LDO2";
			regulator-name = "mt6360,ldo2";
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <3600000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};

		mt6360_ldo3: ldo3 {
			regulator-compatible = "LDO3";
			regulator-name = "mt6360,ldo3";
			regulator-min-microvolt = <1200000>;
			regulator-max-microvolt = <3600000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};

		mt6360_ldo5: ldo5 {
			regulator-compatible = "LDO5";
			regulator-name = "mt6360,ldo5";
			regulator-min-microvolt = <2700000>;
			regulator-max-microvolt = <3600000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};

		mt6360_ldo6: ldo6 {
			regulator-compatible = "LDO6";
			regulator-name = "mt6360,ldo6";
			regulator-min-microvolt = <500000>;
			regulator-max-microvolt = <2100000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};

		mt6360_ldo7: ldo7 {
			regulator-compatible = "LDO7";
			regulator-name = "mt6360,ldo7";
			regulator-min-microvolt = <500000>;
			regulator-max-microvolt = <2100000>;
			regulator-allowed-modes = <MT6360_OPMODE_NORMAL
						   MT6360_OPMODE_LP>;
		};
	};
};
